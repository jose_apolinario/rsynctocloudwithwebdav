#!/bin/bash
# This is my backup script for given folder - created by Jose Teixeira Jul 2015

echo "############### bkupTarGivenFolder.sh ####################"
echo "initialy created by jteixeira on Jul 2015"
echo "last change on 24 Feb 2016 - new technique to pre-bkup with tar"
echo ""
echo "bkupTarGivenFolder.sh [PASSED_DIR - to be backed up] [TAR_FILENAME - Wished bkup tar filename]"
echo ""



ARGV0=$0 # First argument is shell command (as in C)
#echo "Command: $ARGV0"

ARGC=$#  # Number of args, not counting $0
#echo "Number of args: $ARGC"

# Check if arguments were passed 
if [ $ARGC -eq 0 ]
  then
    echo "No arguments supplied"
fi

# Check if the passwd directory (ARGV1)  is a directory 

PASSED_DIR=$1

if   [ -d "${PASSED_DIR}" ]
then echo "\[ ${PASSED_DIR} \] is a directory - bkup can continue";
elif [ -f "${PASSED_DIR}" ]
then echo "ERROR ${PASSED_DIR} is a file";
    exit 1 
else echo "ERROR ${PASSED_DIR} is not valid";
     exit 1
fi


# Check if TAR_FILENAME (ARGV2) is not an empty string 

TAR_FILENAME=$2

if [ -z "$2" ]
  then
    echo "No TAR_FILENAME argument supplied"
fi


# let's create a variable for the log file, let's also name the log file with the filename and timestamp it
LOG=TNSK_BackupLog_"$TAR_FILENAME".log.txt
echo LOG="$LOG"


# start the backup, create a log file which will record any messages run by this script
echo ""
echo  "Starting backup of $PASSED_DIR to $TAR_FILENAME directory with logs to  $LOG" | tee "$LOG"
echo ""

# compress the directory and files, direct the tar.gz file to your destination directory
echo "invoking" \[ tar -czvf "$TAR_FILENAME"  "$PASSED_DIR" \] 
echo ""

#Invokes tar
echo ""
echo tar -czvf  "$TAR_FILENAME"   "$PASSED_DIR"  | tee -a "$LOG"
echo            "*********************************************" | tee -a "$LOG"
echo tar -czvf  "*** Long entries created by tar start here***" | tee -a  "$LOG"
tar -czvf "$TAR_FILENAME" "$PASSED_DIR"  | tee -a "$LOG"
echo tar -czvf  "*** Long entries created by tar end here***"   | tee -a "$LOG"
echo            "*******************************************" | tee -a "$LOG"
echo ""
echo ""

TIMESTAMP=`date +%Y-%b-%d_%H:%M`
 
# end the backup, append to log file created by this script
echo  "Ending backup of $PASSED_DIR at $TIMESTAMP" | tee -a  "$LOG"


# Also they should be added to the exclude dir 
# Everything to not be bkudep should be simple as put in a folder named . TNSK or startning with TNSK


echo "" 
echo "Bkup is DONE. SUCESSES in saving stuff"
echo ""

echo "Taking the chance to remove old BKUP and LOG files" 
# Remove backup files older than 90 days in the given folder and current path  
#echo RUNNING: find $PASSED_DIR -maxdepth 1 -name "'TNSK_Backup*'"  -mtime +90 -delete
# From the PASSED DIR
find "$PASSED_DIR" -maxdepth 1 -name "'TNSK_Backup*'" -mtime +90 -delete | tee -a "$LOG" & 
# From current folder
find . -maxdepth 1 -name "'TNSK_Backup*'" -mtime +90 -delete  | tee -a "$LOG" & 

# Deletes all TNSK_Backup in current folder - Keep it commented
#echo find . -maxdepth 1 -name "'TNSK_Backup*'"  -delete
#find . -maxdepth 1 -name "'TNSK_Backup*'"  -delete  

echo "Older bkup file removed at $TIMESTAMP" 
echo "Older bkup file removed at $TIMESTAMP"  | tee -a  "$LOG"

 
# Remove backup files older than 90 days in all home folder - Takes lot of time - Keep it commented 
#find /home/apolinex -name "'TNSK_Backup*'" -mtime +90 -delete >> "$LOG" & 
#echo "Older bkup file removed at $TIMESTAMP"  >> "$LOG"

# Remove all bkup files DANGEROUS SCRIPT - Keep it commented
#find /home/apolinex -name "'TNSK_Backup*'" -delete  
#echo "All TNSK_Backup files were deleted" 
echo ""
echo \################ bkupTarGivenFolder.sh without any found errors ####################
echo ""
 
