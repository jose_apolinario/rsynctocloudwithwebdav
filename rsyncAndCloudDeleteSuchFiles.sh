#!/bin/bash

# Script that keeps a list of files, and deletes them locally or remotely !!! Warning !!.
# Deletes the listed files in the base of  a local folder  or a cloud remote folder (e.g. owncube) 
# Run both in /home/apolinex/ownCloud or in /home/apolinex/owncloudDAV to really get ride of such files 

echo "############### .rsyncAndCloudDeleteSuchFiles.sh ####################"
echo "initialy created by jteixeira on 5 Nov 2016"
echo "last change on 5 Nov 2016 - fist working version"
echo ""
echo "Deleting some files "
echo ""

# We store the arguments from bash command line in special array
args=("$@")


echo This will delete some files from your owncloudsync folders 


read -p "Are you sure? (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi


echo ""
echo "Then, you should do it first localy and then remotely on the webdav."
echo "After running in all machine, everything will be delelted and not rsynked again." 

sleep 2 


echo ""
echo "Veryfing hostname"
echo HOSTNAME = `hostname`

case `hostname` in
  (dell-notebook-utu)      
	printf '%s\n' "Recognized hostname - delete can continue"    
	LOCAL_DIR="/media/USB-HD/ownCloud"
	OWNCLOUD_DIR="/home/apolinex/owncloudDAV"
	;;
  (delllatitudee6410)
      	printf '%s\n' "Recognized hostname - delete can continue"    
	LOCAL_DIR="/home/apolinex/ownCloud"
	OWNCLOUD_DIR="/home/apolinex/owncloudDAV"
	;;
  (apolinexnetbookpro)
	printf '%s\n' "Recognized hostname - delete can continue"    
	LOCAL_DIR="/home/apolinex/ownCloud"
	OWNCLOUD_DIR="/home/apolinex/owncloudDAV"
	;; 
  (*)   printf '%s\n' "Unrecognized hostname - delete can't continue"
	exit 
esac




echo ""
echo Testing current working directory -- $(pwd) -- 



case $(pwd) in
  ($LOCAL_DIR)      
	printf '%s\n' "It's the local dir base - delete can continue"    
	;;
  ($OWNCLOUD_DIR)
      	printf '%s\n' "It's the remote dir base - delete can continue"    
	;;
   (*)   printf '%s\n' "Can only be run in the base dir of an rsynOwnCloud instance (e.g., /home/apolinex/ownCloud or in /home/apolinex/owncloudDAV)- delete can't continue"
	exit 
esac




echo ""
echo "Testing if webdav volume is mounted"

mountpoint $OWNCLOUD_DIR
if [ $? -eq 0 ] ; then
echo "$OWNCLOUD_DIR WEBDAV is mounted"
else
echo "$OWNCLOUD_DIR WEBDAV is not mounted"
echo "Mounting dav volume" 
mount $OWNCLOUD_DIR
fi




echo "Deleting the following folders / files"

rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Crownstone"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Feller"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Fitzgerald"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Hansen"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Howison"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Lener"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Monteiro"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Saker"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Tirole"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/Wade"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/West"
rm -vfr "./Documentation/Research/PHD Thesis writing/MustCiteScholars/von Hipel"
rm -vfr "./Documentation/Research/PHD Thesis writing/PHD Thesis writing"
rm -vfr "./Documentation/Institutional/Kela/Childcare leaves/Application Alda v - 6 Jan 2015/Application Alda v - 6 Jan 2015"
rm -vfr "./Documentation/Institutional/Kela/Childcare leaves/untitled folder"
rm -vfr "./Documentation/Research/To develop/OpenStackSNA/scientific papers/Working-papers/Working-papers" 


echo "" 
echo "DONE"
echo "All files marked in this file were deleted"  
echo ""


echo "Do you want to take the chance to remove TNSK backup and log files from all over"? 


read -p "Remove all TNSK backup and log files (y/n)" -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi

echo "" 
echo "NOT implemented yet"
echo "Fist make sure that photos are ok "
