#!/bin/bash



# Script that rsync local folder (given as argument) to a clowd remote folder (e.g. owncube) 
# Bkup is not created by passing the option -nb 

echo "############### rsyncGivenFolderToCloud.sh ####################"
echo "initialy created by jteixeira on 23 Sep 2015"
echo "Last change 2 Jan - moving to https://jteixeira.hostiso.host/remote.php/webdav/ webdav provider"
echo "Changed: 24 Feb 2016 - new technique to pre-bkup with tar"
echo ""
echo "Syncronizing the given local folder TO the remote cloud folder (master server)"
echo ""

# We store the arguments from bash command line in special array
args=("$@")

# first arg 

#echo "firt arg=" ${args[0]} 

# second arg
#echo ${args[1]}

# Check if the NO PRE BACKUP option was passed (-nb)
# set bkup_should_not_be_created bolean as true 
for i in "$@" ; do
    if [[ $i == "-nb" ]] ; then
	echo ""
	echo "-nb parameter detected"
	echo ""
	echo "WARNING: PRE-BKUP will not be created"
	echo ""
	bkup_should_not_be_created=true
        break
    fi
done


# Assign the given folder
# first argument if "rsyncGivenFolderToCloud.sh Folder"
# 2nd argument if "rsyncGivenFolderToCloud.sh -nb Folder" 



if [ "$bkup_should_not_be_created" = true ] ; then
    #  if -nb option was passed we have an extra argument 
    GIVEN_LOCAL_FOLDER=${args[1]}
else
    # otherwise folder to be syncronized should be the first arguemnt 
    GIVEN_LOCAL_FOLDER=${args[0]}
fi




# Test if the given folder exits 
echo "GIVEN_LOCAL_FOLDER"=$GIVEN_LOCAL_FOLDER

# bash check if directory exists
if [ -d "$GIVEN_LOCAL_FOLDER" ]; then
    echo GIVEN_LOCAL_FOLDER EXISTS - sync can continue #\[ "$GIVEN_LOCAL_FOLDER" \]
    #echo "Given folder exists"
else 
	echo "Given folder does not exists"
	exit 
fi 


echo ""
echo "Veryfing hostname"
echo HOSTNAME = `hostname`

case `hostname` in
  (dell-notebook-utu)      
	printf '%s\n' "Recognized hostname - sync can continue"    
	#LOCAL_DIR="/media/USB-HD/ownCloud"
	#OWNCLOUD_DIR="/home/apolinex/owncloudDAV"
	echo "Define hostname local and remote first"
	exit 
	;;
  (delllatitudee6410)
      	printf '%s\n' "Recognized hostname - sync can continue"    
	#LOCAL_DIR="/home/apolinex/ownCloud"
	#OWNCLOUD_DIR="/home/apolinex/owncloudDAV"
	LOCAL_DIR="/home/apolinex/nextCloudLOCAL"
	OWNCLOUD_DIR="/home/apolinex/nextCloudDAV"
	;;
  (apolinexnetbookpro)
	printf '%s\n' "Recognized hostname - sync can continue"    
	#LOCAL_DIR="/home/apolinex/ownCloud"
	#OWNCLOUD_DIR="/home/apolinex/owncloudDAV"
	echo "Define hostname local and remote first"
	exit
	;; 
  (*)   printf '%s\n' "Unrecognized hostname - sync can't continue"
	exit 
esac


echo ""
echo "Testing if webdav volume is mounted"

mountpoint $OWNCLOUD_DIR
if [ $? -eq 0 ] ; then
echo "$OWNCLOUD_DIR WEBDAV is mounted"
else
echo "$OWNCLOUD_DIR WEBDAV is not mounted"
echo "Mounting dav volume" 
mount $OWNCLOUD_DIR
fi

sleep 2



echo "Sync from local folder" "$GIVEN_LOCAL_FOLDER" to remote webdav location mounted at $OWNCLOUD_DIR



if [ "$bkup_should_not_be_created" = true ] ; then

    echo ''
    echo 'Skipping  the pre backup process'
    echo '' 

else 
    # Test if bkup script  exists 
    # Makes ./bkupTarGivenFolder.sh $GIVEN_LOCAL_FOLDER  possible 

    echo ''
    echo 'Starting the pre-backup process'
    echo '' 


    
    BKUP_SCRIPT=$LOCAL_DIR/SharedScripts/bkupTarGivenFolder.sh

    if [ -e $BKUP_SCRIPT ]
	then echo Backing-up using script "$BKUP_SCRIPT" 
	else 
	    echo "Can't access bkup script"
	    echo "$BKUP_SCRIPT is not a regular file"
	    echo "ABORTED"
	   exit 
    fi

    echo ""
    echo "First of all create a bkup of the given folder $GIVEN_LOCAL_FOLDER"
    echo "" 


    TIMESTAMP=`date +%Y-%b-%d_%Hh%Mm%Ss`

    SRC_BASENAME=`basename "$GIVEN_LOCAL_FOLDER"`   
    #echo  SRC_BASENAME=$SRC_BASENAME

    BKUP_FNAME="TNSK_Backup_BEFORE_SENDING_2_CLOUD"$SRC_BASENAME"_$TIMESTAMP.tar.gz"
    #echo BKUP_FNAME = "$BKUP_FNAME"

    echo "Creating bkup file named $BKUP_FNAME"
    echo ""
    
    echo "Starting backup in 2 seconds"
    sleep 1
    echo "Starting backup in 1 seconds"
    sleep 1
    
    # Debugging the bkup script call
    echo "Starting backup by invoking:" 
    echo "$BKUP_SCRIPT" "$GIVEN_LOCAL_FOLDER" "$BKUP_FNAME"
    echo ""

    
    
    # Calls the bkup script 
    "$BKUP_SCRIPT" "$GIVEN_LOCAL_FOLDER" "$BKUP_FNAME"

    # Test if the tar pre-bkup was created, othewise abort
    if [ -f "$BKUP_FNAME" ]; then
	echo Tar pre-backup existes - sync can continue
    else 
	    echo "Can't access pre-bkup tar file"
	    echo "$BKUP_FNAME does not exist "
	    echo "ABORTED"
	    exit 
    fi

    # Test if the tar pre-bkup file is empty, otherwise abort 
    if [ -s "$BKUP_FNAME" ]; then
	echo Tar pre-backup have some data - sync can continue
    else 
	    echo "Pre-bkup tar file is empty"
	    echo "$BKUP_FNAME does not have data "
	    echo "ABORTED"
	    exit 
    fi
    
    # Test that the pre-bkup is not corrupt, otherwise abort 
    if ! tar tf "$BKUP_FNAME" &> /dev/null; then
	    echo "Pre-bkup tar file is corrupt"
	    echo "$BKUP_FNAME is corrupt "
	    echo "ABORTED"
	    exit
    fi


    echo ""
    echo "Bkup was created. Time to sync!\n"
    echo ""
    
fi


#echo LOCAL FOLDER = \[ "$GIVEN_LOCAL_FOLDER"  \] 

#Get the full local path for the given folder 
LOCAL_FOLDER_FULL_PATH=`readlink -f "$GIVEN_LOCAL_FOLDER"`

echo LOCAL_FOLDER_FULL_PATH=$LOCAL_FOLDER_FULL_PATH

# Now get the relative path by removing the local prefix 


prefix=$LOCAL_DIR
string=$LOCAL_FOLDER_FULL_PATH
string=${string#$prefix}

LOCAL_FOLDER_REL_PATH=$string
echo LOCAL_FOLDER_REL_PATH=$LOCAL_FOLDER_REL_PATH

# PRINT the remote target 

echo OWNCLOUD REMOTE WEBDAV PATH = \[ "$OWNCLOUD_DIR$LOCAL_FOLDER_REL_PATH/" \]


# Test if exclude file exists 
# --exclude-from=$EXCLUDE_FILE 
#EXCLUDE_FILE=$OWNCLOUD_DIR/SharedConfigurations/rsync-exclude.txt
EXCLUDE_FILE=$LOCAL_DIR/SharedConfigurations/rsync-exclude.txt

if [ -f $EXCLUDE_FILE ]
	then echo Exclude file configuations from $EXCLUDE_FILE
	else 
	    echo "Can't access to file configuratuons"
	    echo "$EXCLUDE_FILE is not a regular file"
	    echo "ABORTED"
	   exit 
    fi


# Sets the Log filename


LOG_FILE=$HOME/TNSKrsyncToCloud."$SRC_BASENAME".$(date +%F:%H:%M:%S).log 
echo LOG_FILE == $LOG_FILE 



echo "" 
echo RUNNING  the following rsync cmd:
echo "" 




# Diplays rsync args
echo rsync -vv -r --progress --human-readable  --protect-args --times --update  --compress --cvs-exclude  --size-only --no-whole-file --inplace --info=all2  --progress --log-file="$LOG_FILE" --exclude-from=$EXCLUDE_FILE  "$GIVEN_LOCAL_FOLDER/" "$OWNCLOUD_DIR$LOCAL_FOLDER_REL_PATH/"



echo "STARTING rsync in 3 seconds"

sleep 1

echo "STARTING rsync in 2 seconds"

sleep 1

echo "STARTING rsync in 1 seconds"

sleep 1 

# Exec rsync 
rsync -vv -r --progress --human-readable  --protect-args --times --update --compress --cvs-exclude  --size-only --no-whole-file --inplace --info=all2  --progress --log-file="$LOG_FILE" --exclude-from=$EXCLUDE_FILE "$GIVEN_LOCAL_FOLDER/" "$OWNCLOUD_DIR$LOCAL_FOLDER_REL_PATH/"  




